#!/bin/bash
fragmentation_by_mounth(){
local str=`eval "expr \"\$"$1"\" "`; 
mounth_str=`echo $str | sed "s/^\(.\{3\}\).*$/\1/g"`;
case "$mounth_str" in
'Jan' )
mounth_num=1;
;;
'Feb' )
mounth_num=2;
;;
'Mar' )
mounth_num=3;
;;
'Apr' )
mounth_num=4;
;;
'May' )
mounth_num=5;
;;
'Jun' )
mounth_num=6;
;;
'Jul' )
mounth_num=7;
;;
'Aug' )
mounth_num=8;
;;
'Sep' )
mounth_num=9;
;;
'Oct' )
mounth_num=10;
;;
'Nov' )
mounth_num=11;
;;
'Dec' )
mounth_num=12;
;;
esac
str=`echo $str | sed "s/^.*\(.\{11\}\)$/\1/g"`;
eval "$1=\"$mounth_num $str\"";
}

compare_strs_length(){
local str1=`eval "expr \"\$"$1"\" "`; 
local str2=`eval "expr \"\$"$2"\" "`; 
fragmentation_by_mounth str1;
fragmentation_by_mounth str2;
IFSorigin=$IFS;
IFS=$' '$':';
str1=( `echo "$str1"` );
str2=( `echo "$str2"` );
for (( i=0; i<5; i++))
do
if [ "${str1[$i]}" -gt "${str2[$i]}" ]

	then
	compare_result=MORE;
	break;
else
	if [ "${str1[$i]}" -lt "${str2[$i]}" ]
		then
		compare_result=LESS;
		break;
	else
		compare_result=EQUAL;
	fi
fi
done
IFS=$IFSorigin;
eval "$3=\"$compare_result\"";
}

search_logs_from_file(){
local file_name=`eval "expr \"\$"$1"\" "`; 
local process_name=`eval "expr \"\$"$2"\" "`;
while read line
do
date_str=`echo $line | sed "s/^\(.\{15\}\).*$/\1/g"`; #отрезали 15 знаков
fragmentation_by_mounth date_str;
compare_strs_length date_str right_period_time result1;
compare_strs_length date_str left_period_time result2;
if [[ "$result1" = "LESS" || "$result1" = "EQUAL" ]]
then
	if [[ "$result2" = "MORE" || "$result2" = "EQUAL" ]]
		then
		(echo "$line" | grep "$process_name") > /dev/null;
		result=$?;
		if (($result==1))
		then
			continue;
		else
			echo $line;
		fi
	else 
		continue;
	fi
else 
	continue;
fi 
done < $1
}
echo "Скрипт для разбора логов. Введите имя процесса: ";
read process;
echo "Необходимо ввести промежуток времени, за который следует найти логи. "
#echo "Введите левую границу интервала. Например: Jul 29 11:40:06";
#read left_period_time;
left_period_time="Aug  2 15:57:00"
fragmentation_by_mounth left_period_time;
#echo "Введите правую границу интервала. Например: Jul 31 11:40:06"
#read right_period_time;
right_period_time="Aug  2 16:00:00"
fragmentation_by_mounth right_period_time;
cd /var/log;
(find /var/log -maxdepth 1 -type f -name $process | grep $process) > /dev/null;
result=$?;
if (($result==0))
then
	echo "Файл с названием процесса найден. Начинаем поиск логов."
	search_logs_from_file process process;
	
else
	echo "Файл с названием процесса не найден. "
	(find /var/log -maxdepth 1 -type d -name $process | grep $process) > /dev/null;
	result=$?;
	if (($result==0))
	then
		echo "Каталог с названием процесса найден. Начинаем поиск логов.";
		cd $process;
		mas=( `find /var/log -maxdepth 1 -type f` );
		num_elem=${#mas[@]};
		for (( i=0; i<$num_elem; i++))
		do
			search_logs_from_file {mas[$i]} process;
		done
	else
		echo "Каталог с названием процесса не найден. ";
		echo "Начинаем поиск логов в syslog";
		pwd;
		search_logs_from_file syslog process;
	fi
fi


